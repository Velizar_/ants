import System.Environment (getArgs)
import Control.Monad (forM, forM_, foldM)
import Data.Array.IO (IOArray, readArray, getBounds, thaw)
import Data.List (isInfixOf)
import Data.Char (digitToInt)
import Data.List.Utils (replace)
import qualified Data.Text as T
import qualified Data.Array as A
import qualified Data.Set as S

import Action

newAnt :: ID -> Color -> Ant
newAnt i color = Ant i color 0 0 E False

newAnthill :: ID -> Color -> Cell
newAnthill k color = Clear (Just $ newAnt k color) (Just color) S.empty S.empty 0

parseCell :: Char -> ID -> Cell
parseCell '#' _ = Rocky
parseCell '.' _ = Clear Nothing Nothing S.empty S.empty 0
parseCell '+' k = newAnthill k Red
parseCell '-' k = newAnthill k Black
parseCell n _ = Clear Nothing Nothing S.empty S.empty (digitToInt n)

parseCellAccId :: ID -> Char -> (Cell, ID)
parseCellAccId k cellRaw = (cell, newId cell)
    where
        cell = parseCell cellRaw k
        newId Clear { _ant = (Just _) } = k + 1
        newId _ = k

parseRow :: Int -> ID -> String -> IO (Row, ID)
parseRow sz nextID rowRaw = let
    rowRaw' = filter (/= ' ') rowRaw
    iterateCell :: ([Cell], Int) -> Char -> ([Cell], Int)
    iterateCell (prevCells, nextID) raw =
        let (nextCell, nextID') = parseCellAccId nextID raw
        in (nextCell : prevCells, nextID')
    (rowsRv, lastID) = foldl iterateCell ([], nextID) rowRaw'
    in do
        row <- thaw (A.listArray (1, sz) $ reverse rowsRv)
        return (row, lastID)

-- output: IO (world, anthillCount)
parseWorld :: String -> IO (World, Int)
parseWorld raw = do
    (rowsRv, anthillCount) <- rowsIO
    world <- thaw (A.listArray (1, n) $ reverse rowsRv)
    return (world, anthillCount)
    where
        mStr : nStr : rowsRaw = lines raw
        m = read mStr :: Int
        n = read nStr :: Int
        iterateRow :: ([Row], ID) -> String -> IO ([Row], ID)
        iterateRow (prevRows, nextID) rowRaw = do
            (nextRow, nextID') <- parseRow m nextID rowRaw
            return (nextRow : prevRows, nextID')
        rowsIO = foldM iterateRow ([], 0) rowsRaw

parseAntInstructions :: String -> AntInstructions
parseAntInstructions rawInput = A.listArray (0, size - 1) list
    where size = length $ lines rawInput
          -- Hack warning: the fixes depend on the current structure of the state input,
          --   rather than the underlying problem and are sensitive to specification
          --   changes. This can be fixed by using regex instead.
          fixLeft = replace "Left " "L "
          fixRight = replace "Right " "R "
          fixMarker line
              | isInfixOf " Marker " line = replaced
              | otherwise                 = line
                  where replaced = (replace "Marker" "(Marker" line) ++ ")"
          fixLine = fixLeft . fixRight . fixMarker
          list = map ((\line -> read line :: Instruction) . fixLine) $ lines rawInput

startAntPositions :: World -> IO AntPositions
startAntPositions world = do
    worldList <- (arrayToList world) >>= mapM arrayToList
    let antPosList = [Just (j, i) |
                      (i, row) <- zip [1..] worldList,
                      (j, cell) <- zip [1..] row,
                      hasAnt cell]
    thaw $ A.listArray (0, (length antPosList) - 1) antPosList

arrayToList :: IOArray Int a -> IO [a]
arrayToList ar = do
    (i, j) <- getBounds ar
    mapM (readArray ar) [i..j]

-- converts to string via show
debugWorld :: World -> IO ()
debugWorld world = do
    rowsList <- (arrayToList world) >>= mapM arrayToList
    mapM_ (putStrLn . (++ "\n") . show) rowsList

numberOfTurns = 1000

main = do
    [worldStr, redAntStr, blackAntStr] <- getArgs >>= mapM readFile
    (world, antCount) <- parseWorld worldStr
    antPositions <- startAntPositions world
    let redInstructions = parseAntInstructions redAntStr
        blackInstructions = parseAntInstructions blackAntStr

    -- for each turn:
    --   for each antPos:
    --     call antTurn
    -- also pass RNG seed to next iteration
    foldM (\outerSeed _ -> do
        foldM (\thisSeed antID ->
            antTurn antID redInstructions blackInstructions thisSeed world antPositions
            ) outerSeed [0..(antCount - 1)]
        ) startSeed [1..numberOfTurns]

    -- debug
    debugWorld world
    (arrayToList antPositions) >>= print
