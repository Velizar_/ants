{-# LANGUAGE TemplateHaskell #-}

module Action where
    import Data.Array.IO (IOArray, readArray, writeArray)
    import Data.Set (Set)
    import Control.Lens (makeLenses, over, _Just)
    import Control.Monad (when)
    import qualified Data.Array as A
    import qualified Data.Set as S

    data Color = Red | Black deriving (Show, Eq)

    otherColor Red = Black
    otherColor Black = Red

    -- ID is 0-indexed
    -- everything else is 1-indexed
    type ID = Int
    type Row = IOArray Int Cell
    type World = IOArray Int Row

    type AntPositions = IOArray ID (Maybe Pos)

    type State = Int

    -- (x, y)
    -- (world !! y) !! x = cell at pos (x, y)
    type Pos = (Int, Int)

    -- changed from `Left | Right` to avoid conflict with Data.Either
    data LeftOrRight = L | R deriving (Show, Read)

    -- corresponding to directions 0 | 1 | 2 | 3 | 4 | 5 in specification
    data Dir = E | SE | SW | W | NW | NE deriving (Show, Enum)

    -- should be in 0..5
    type MarkNum = Int

    nextDir :: Dir -> Dir
    nextDir NE = E
    nextDir d = succ d

    prevDir :: Dir -> Dir
    prevDir E = NE
    prevDir d = pred d

    data Condition = Friend | Foe | FriendWithFood | FoeWithFood | Food | Rock | Marker MarkNum | FoeMarker | Home | FoeHome deriving (Show, Read)
    data SenseDir = Here | Ahead | LeftAhead | RightAhead deriving (Show, Read)

    data Instruction = 
        Sense SenseDir State State Condition |
        Mark Int State |
        Unmark Int State |
        PickUp State State |
        Drop State |
        Turn LeftOrRight State |
        Move State State |
        Flip Integer State State deriving (Show, Read)

    isFlip :: Instruction -> Bool
    isFlip (Flip _ _ _) = True
    isFlip _ = False

    type AntInstructions = A.Array Int Instruction

    data Ant = Ant { _idNum :: Int
                   , _color :: Color
                   , _state :: State
                   , _resting :: Int
                   , _direction :: Dir
                   , _hasFood :: Bool
                   } deriving (Show)
    restTurns = 14
    type Food = Int
    type Anthill = Color
    type RedMarker = Set Int
    type BlackMarker = Set Int
    data Cell = Rocky | Clear { _ant :: Maybe Ant
                              , _anthill :: Maybe Anthill
                              , _redMarker :: RedMarker
                              , _blackMarker :: BlackMarker
                              , _food :: Food
                              } deriving (Show)

    makeLenses ''Ant
    makeLenses ''Cell

    -- random - as per 2.10 in specification

    type RNGSeed = Integer

    randomInt' :: Integer -> RNGSeed -> Integer
    randomInt' n s = s `div` 65536 `mod` 16384 `mod` n

    nextSeed :: RNGSeed -> RNGSeed
    nextSeed s = s * 22695477 + 1

    startSeed :: RNGSeed
    startSeed = iterate nextSeed 12345 !! 4

    adjacentCell :: Pos -> Dir -> Pos
    adjacentCell (x, y) E = (x + 1, y)
    adjacentCell (x, y) W = (x - 1, y)
    -- +1 to x if y is odd
    -- looks different from spec because we use 1-indexed world
    adjacentCell (x, y) dir
        | odd y    = case dir of
            SE -> (x,     y + 1)
            SW -> (x - 1, y + 1)
            NW -> (x - 1, y - 1)
            NE -> (x,     y - 1)
        | otherwise = case dir of
            SE -> (x + 1, y + 1)
            SW -> (x,     y + 1)
            NW -> (x,     y - 1)
            NE -> (x + 1, y - 1)

    adjacentCells :: Pos -> [Pos]
    adjacentCells pos = map (adjacentCell pos) [E .. NE]

    turn :: LeftOrRight -> Dir -> Dir
    turn L = prevDir
    turn R = nextDir

    sensedCell :: Pos -> Dir -> SenseDir -> Pos
    sensedCell p _ Here = p
    sensedCell p d Ahead = adjacentCell p d
    sensedCell p d LeftAhead = adjacentCell p (turn L d)
    sensedCell p d RightAhead = adjacentCell p (turn R d)

    hasAnt :: Cell -> Bool
    hasAnt Clear { _ant = Just _ } = True
    hasAnt _ = False

    hasAntColor :: Cell -> Color -> Bool
    hasAntColor Clear { _ant = Just Ant { _color = col' } } col = col == col'
    hasAntColor _ _ = False

    hasAntWithFood :: Cell -> Bool
    hasAntWithFood Clear { _ant = Just Ant { _hasFood = hasFood' } } = hasFood'
    hasAntWithFood _ = False

    checkMarkerAt :: Cell -> Color -> Int -> Bool
    checkMarkerAt Clear { _redMarker = marker } Red markerNum     = S.member markerNum marker
    checkMarkerAt Clear { _blackMarker = marker } Black markerNum = S.member markerNum marker
    checkMarkerAt _ _ _ = False

    checkAnyMarker :: Cell -> Color -> Bool
    checkAnyMarker Clear { _blackMarker = marker } Red = not $ S.null marker
    checkAnyMarker Clear { _redMarker = marker } Black = not $ S.null marker
    checkAnyMarker _ _ = False

    cellMatches :: Cell -> Condition -> Color -> Bool
    cellMatches Rocky Rock _ = True
    cellMatches Rocky _ _ = False
    cellMatches _ Rock _ = False
    cellMatches cell Friend col = hasAntColor cell col
    cellMatches cell Foe col = cellMatches cell Friend (otherColor col)
    cellMatches cell FriendWithFood col = hasAntColor cell col && hasAntWithFood cell
    cellMatches cell FoeWithFood col = cellMatches cell FriendWithFood (otherColor col)
    cellMatches Clear { _food = food' } Food _ = food' > 0
    cellMatches cell (Marker m) col = checkMarkerAt cell col m
    cellMatches cell FoeMarker col = checkAnyMarker cell col
    cellMatches Clear { _anthill = Just col' } Home col = col == col'
    cellMatches Clear { _anthill = Nothing } Home col = False
    cellMatches cell FoeHome col = cellMatches cell Home (otherColor col)

    -- Below are all the actions, implemented by mutating world and antPositions

    getCell :: World -> Pos -> IO (Cell)
    getCell world (x, y) = do
        row <- readArray world y
        readArray row x

    setCell :: World -> Pos -> Cell -> IO ()
    setCell world (x, y) cell = do
        row <- readArray world y
        writeArray row x cell

    updateCell :: World -> Pos -> (Cell -> Cell) -> IO ()
    updateCell world pos updater = do
        cell <- getCell world pos
        setCell world pos (updater cell)

    setState :: World -> Pos -> State -> IO ()
    setState world pos newState =
        updateCell world pos $ over (ant . _Just . state) (const newState)

    markerFunc :: Applicative f => Color -> (RedMarker -> f RedMarker) -> Cell -> f Cell
    markerFunc Red = redMarker
    markerFunc Black = blackMarker

    checkForSurroundedAntAt :: World -> AntPositions -> Pos -> IO ()
    checkForSurroundedAntAt world antPositions pos = do
        cell <- getCell world pos
        case cell of
            Clear { _ant = Just Ant { _color = col } } -> do
                adjacentNum <- adjacentAnts pos (otherColor col)
                when (adjacentNum >= 5) $ killAnt pos
            _ -> return ()
        where adjacentAnts antPos col = do
                  cells <- sequence $ map (getCell world) (adjacentCells antPos)
                  return $ length (filter (\c -> hasAntColor c col) cells)
              killAnt pos = do
                  Clear { _ant = Just Ant { _idNum = antID } } <- getCell world pos
                  updateCell world pos $ over ant (const Nothing)
                  updateCell world pos $ over food (+ 3)
                  writeArray antPositions antID Nothing

    checkForSurroundedAnts :: World -> AntPositions -> Pos -> IO ()
    checkForSurroundedAnts world antPositions pos = do
        checkForSurroundedAntAt world antPositions pos
        sequence_ $ map (checkForSurroundedAntAt world antPositions) (adjacentCells pos)

    antTurn :: ID -> AntInstructions -> AntInstructions -> RNGSeed -> World -> AntPositions -> IO (RNGSeed)
    antTurn antID redInstructions blackInstructions seed world antPositions = do
        maybePos <- readArray antPositions antID
        case maybePos of
            Nothing -> return seed
            Just antPos -> do
                cell @ Clear { _ant = Just thisAnt @ Ant {
                        _color = col,
                        _state = st,
                        _direction = dir,
                        _hasFood = hasFood'
                    }
                } <- getCell world antPos
                if (_resting thisAnt == 0) then do
                    let instruction = case col of Red -> (redInstructions A.! st)
                                                  Black -> (blackInstructions A.! st)
                        outputSeed = if isFlip instruction then nextSeed seed else seed
                    case instruction of
                        Sense senseDir st1 st2 cond -> do
                            targetCell <- getCell world (sensedCell antPos dir senseDir)
                            let st = if cellMatches targetCell cond col then st1 else st2
                            setState world antPos st
                        Mark markNum st -> do
                            updateCell world antPos $ over (markerFunc col) (S.insert markNum)
                            setState world antPos st
                        Unmark markNum st -> do
                            updateCell world antPos $ over (markerFunc col) (S.delete markNum)
                            setState world antPos st
                        PickUp st1 st2 -> do
                            let Clear { _food = food' } = cell
                            if (not hasFood') && food' > 0 then do
                                updateCell world antPos $ over food (subtract 1)
                                updateCell world antPos $ over (ant . _Just . hasFood) (const True)
                                setState world antPos st1
                            else do
                                setState world antPos st2
                        Drop st -> do
                            if hasFood' then do
                                updateCell world antPos $ over food (+ 1)
                                updateCell world antPos $ over (ant . _Just . hasFood) (const False)
                            else
                                return ()
                            setState world antPos st
                        Turn leftOrRight st -> do
                            updateCell world antPos $ over (ant . _Just . direction) (turn leftOrRight)
                            setState world antPos st
                        Move st1 st2 -> do
                            dstCell <- getCell world newPos
                            if cellIsFree dstCell then do
                                updateCell world antPos $ over ant (const Nothing)
                                updateCell world newPos $ over ant (const $ Just thisAntTired)
                                writeArray antPositions antID (Just newPos)
                                checkForSurroundedAnts world antPositions newPos
                                setState world newPos st1
                            else
                                setState world antPos st2
                            where newPos = adjacentCell antPos dir
                                  cellIsFree Clear { _ant = Nothing } = True
                                  cellIsFree _ = False
                                  thisAntTired = over resting (const restTurns) thisAnt
                        Flip n st1 st2 ->
                            setState world antPos st
                            where st = if randomInt' n seed == 0 then st1 else st2
                    return outputSeed
                else do -- ant is resting
                    updateCell world antPos $ over (ant . _Just . resting) (subtract 1)
                    return seed
